<?php
session_start();
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP129317\Name\Name;
use App\Bitm\SEIP129317\Name\Message;
$name = new Name();
$allItem=$name->index();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Name</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <style>
        body {
            margin-left: auto;
            margin-right: auto;
            width: 70%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th{
            padding: 10px;
        }
        td{
            padding: 10px;
            text-align: center;
        }
        a:link {
            text-decoration: none;
        }
        #id3
        {
            background-color:#99CCFF;
        }
        #id4
        {
            background-color:#9999FF;
        }
        #id5
        {
            background-color:#990000;
        }
    </style>
</head>
<body>

    <h2>All Name list</h2>

 <button onclick="window.location.href='create.php'">Add Name</button> <br><br>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>
        <table style="width:110%">
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Action</th>
            </tr>
            <?php
            $sl=0;
            foreach ($allItem as $name){
                $sl++?>
            <tr>
                <td><?php echo $sl?></td>
                <td><?php echo $name->id ?></td>
                <td><?php echo $name->firstname ?></td>
                <td><?php echo $name->middlename ?></td>
                <td><?php echo $name->lastname ?></td>
                <td><button id="id3" onclick="window.location.href='view.php?id=<?php echo $name->id ?>'">View</button>
                    <button id="id4" onclick="window.location.href='edit.php?id=<?php echo $name->id ?>'">Edit</button>
                    <button id="id5" onclick="window.location.href='delete.php?id=<?php echo $name->id ?>'">Delete</button>

                </td>
            </tr>
            <?php }?>
        </table>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

