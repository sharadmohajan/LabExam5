<?php
namespace App\Bitm\SEIP129317\Name;
use App\Bitm\SEIP129317\Name\Message;


Class Name{
    public $id="";
    public $firstname="";
    public $middlename="";
    public $lastname="";
    public $conn;


public function prepare($data="")
{
    if (array_key_exists("firstname", $data)) {
        $this->firstname = $data['firstname'];
    }
    if (array_key_exists("middlename", $data)) {
        $this->middlename = $data['middlename'];
    }
    if (array_key_exists("lastname", $data)) {
        $this->lastname = $data['lastname'];
    }
    if (array_key_exists("id", $data)) {
        $this->id = $data['id'];
    }
    return $this;
}

public function __construct(){
    $this->conn= mysqli_connect("localhost","root","","labxm5b20") or die("Database connection establish failed");
}

    public function store(){
        $query="INSERT INTO `labxm5b20`.`student` (`firstname`, `middlename`, `lastname`) VALUES ('".$this->firstname."', '".$this->middlename."', '".$this->lastname."')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has not been stored successfully.
</div>");
            Utility::redirect('index.php');

        }

    }



    public  function index(){
        $_allItem=array();
        $query= "SELECT * FROM `student`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }

    public function view(){
        $query="SELECT * FROM `student` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `labxm5b20`.`student` SET `firstname` = '".$this->firstname."',`middlename` = '".$this->middlename."',`lastname` = '".$this->lastname."' WHERE `student`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('index.php');

        }

    }

    public function delete()
    {
        $query = "DELETE FROM `labxm5b20`.`student` WHERE `student`.`id` =" . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>DELETED!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('index.php');


        }
    }






}