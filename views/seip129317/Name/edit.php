<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129317\Name\Name;
use App\Bitm\SEIP129317\Name\Utility;
$name= new Name();
$singleItem=$name->prepare($_GET)->view();
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Name</title>
<style>
    body {
        margin-left: auto;
        margin-right: auto;
        width: 70%;
    }
</style>
</head>
<body>

    <h2>Edit Page</h2>
    <form role="form" action="update.php" method="post">
            <label>Edit First Name:</label>
            <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
            <input type="text" name="firstname" class="form-control" id="email" value="<?php echo $singleItem->firstname?>"><br>
        <label>Edit Middle Name:</label>
        <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
        <input type="text" name="middlename" class="form-control" id="email" value="<?php echo $singleItem->middlename?>"><br>
        <label>Edit Last Name:</label>
        <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
        <input type="text" name="lastname" class="form-control" id="email" value="<?php echo $singleItem->lastname?>"><br>
        <button type="submit" class="btn btn-default">Update</button>
    </form>

</body>
</html>