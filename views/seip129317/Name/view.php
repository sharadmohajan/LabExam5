<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129317\Name\Name;
use App\Bitm\SEIP129317\Name\Utility;
$name= new Name();
$singlename=$name->prepare($_GET)->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Name</title>
    <style>
        body {
            margin-left: auto;
            margin-right: auto;
            width: 70%;
        }
        h2 {
            text-indent: 25px;
        }
    </style>
</head>
<body>

    <h2><?php echo $singlename->id ?></h2>
    <ul>
        <li>ID: <?php echo $singlename->id?></li>
        <li>First Name: <?php echo $singlename->firstname ?></li>
        <li>Middle Name: <?php echo $singlename->middlename ?></li>
        <li>Last Name: <?php echo $singlename->lastname ?></li>

    </ul>

</body>
</html>

